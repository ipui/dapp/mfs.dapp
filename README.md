# key.dapp

single purpose dapp to manage your ipfs [mutable file system](http://docs.ipfs.io/concepts/file-systems/#mutable-file-system-mfs).

> **this is not an implementation of an entire ipfs node**, this project only use the `api` that the _daemon_ binds over the port `5001` (by default). [go](https://github.com/ipfs/go-ipfs) or [javascript](https://github.com/ipfs/js-ipfs) implementation is by your own.

## ip[fn]s links
* [/ipns/QmavyUkXD48fi5mM4CNAiQL6GyV87bRt2tm9XtqPvYMHg9](https://ipfs.io/ipns/QmavyUkXD48fi5mM4CNAiQL6GyV87bRt2tm9XtqPvYMHg9) (permanent)
* [mfs.dapp.rabrux.space](http://mfs.dapp.rabrux.space) (permanent)
* [/ipfs/QmaUTsWR5gNFMNeT2NybtmhmWfY9k27ztagqBHqSg1izNV](https://ipfs.io/ipfs/QmaUTsWR5gNFMNeT2NybtmhmWfY9k27ztagqBHqSg1izNV) latest

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)

## dev

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
