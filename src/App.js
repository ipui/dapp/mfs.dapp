import React from 'react';
import './skin/index.sass';
import Ipfs from './ipfs';
import NoIpfs from './noIpfs';
import Clipboard from './clipboard';
import Directory from './directory';
import TopBar from './topBar';
import DirectoryList from './directoryList';
import BottomBar from './bottomBar';
import { homepage, bugs } from '../package.json';

function App() {
  return (
    <Ipfs noIpfs={ NoIpfs } links={ { homepage, issues: bugs.url } } >
      <Clipboard localKey="filesClip">
        <Directory>
          <TopBar />
          <DirectoryList />
          <BottomBar />
        </Directory>
      </Clipboard>
    </Ipfs>
  );
}

export default App;
