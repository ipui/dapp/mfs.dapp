import React from 'react'
import { withDirectory } from '../../directory'

import { FiX } from 'react-icons/fi'
import { FaCircle, FaRegCircle, FaRegDotCircle } from 'react-icons/fa'

function PickControl( props ) {

  function pick() {
    const { list, all } = props.withDirectory
    const totalSelected = getTotalSelected()

    if ( totalSelected === list.length )
      return all( false )

    all()
  }

  function closePick() {
    const { closePick: closeIt, all } = props.withDirectory
    all( false )
    closeIt()
  }

  function getTotalSelected() {
    const { list } = props.withDirectory
    return list.reduce( ( total, current ) => {
      if ( current.selected )
        total++
      return total
    }, 0 )
  }

  function getStatusIcon() {
    const { list } = props.withDirectory
    const totalSelected = getTotalSelected()

    if ( totalSelected === 0 )
      return <FaRegCircle />

    if ( totalSelected === list.length )
      return <FaCircle />

    return <FaRegDotCircle />
  }

  const statusIcon = getStatusIcon()

  return (
    <header className="space-between">
      <button onClick={ pick }>
        { statusIcon }
      </button>

      <button onClick={ closePick }>
        <FiX />
      </button>
    </header>
  )
}

export default withDirectory( PickControl )
