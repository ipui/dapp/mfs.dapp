import React, { useState } from 'react';
import IpfsConnect from '../ipfsConnect';
import { withDirectory } from '../directory'
import Breadcrumb from './breadcrumb';
import PickControl from './pickControl'

import { FiSettings, FiX } from 'react-icons/fi';

function TopBar( props ) {
  const [ showSettings, setShowSettings ] = useState( false );

  function toggleSettings() {
    setShowSettings( !showSettings );
  }

  const { isPick } = props.withDirectory

  return (
    <>
      { isPick ? (
        <PickControl />
      ) : (
        <>
          <header>
            <Breadcrumb />

            <button onClick={ () => toggleSettings() }>
              { showSettings ? ( <FiX /> ) : ( <FiSettings /> ) }
            </button>
          </header>

          { showSettings ? (
            <header>
              <IpfsConnect />
            </header>
          ) : null }
        </>
      ) }
    </>
  );
}

export default withDirectory( TopBar );
