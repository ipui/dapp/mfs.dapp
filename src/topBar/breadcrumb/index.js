import React, { useState } from 'react';
import { withDirectory } from '../../directory';
import './breadcrumb.sass';

import { FiCornerLeftUp, FiX } from 'react-icons/fi';

function Breadcrumb( props ) {
  const [ showBreadcrumbs, setShowBreadcrumbs ] = useState( false );
  const { current } = props.withDirectory;
  const crumbs = [ 'root' ].concat( current.dirname
    .split( '/' )
    .filter( el => el )
  )

  function toggle() {
    if ( current.name === '/' ) return setShowBreadcrumbs( false )

    setShowBreadcrumbs( !showBreadcrumbs );
  }

  function goUp() {
    const { dirname } = current;
    const { cd } = props.withDirectory;
    cd( dirname );
  }

  function go( index ) {
    const { cd } = props.withDirectory;

    if ( index === 0 ) {
      cd( '/' );
    } else {
      const dirs = crumbs.slice( 0, index + 1 )
      dirs.shift()
      const to = '/' + dirs.join( '/' );
      cd( to );
    }

    toggle();
  }

  const crumbList = crumbs
    .map( ( el, index ) => (
      <label key={ index + el } onClick={ () => go( index ) }>
        { el }
      </label>
    ) )

  return (
    <>
      { showBreadcrumbs ? (
        <>
          <div className="breadcrumb">
            <div className="scroller">
              { crumbList }
              <label>{ current.name }</label>
            </div>
          </div>
          <button onClick={ toggle }>
            <FiX />
          </button>
        </>
      ) : (
        <>
          { current.name !== '/' ? (
            <button onClick={ () => goUp() }>
              <FiCornerLeftUp />
            </button>
          ) : null }

          <h1 onClick={ toggle }>{ current.name }</h1>
        </>
      ) }
    </>
  );
}

export default withDirectory( Breadcrumb );
