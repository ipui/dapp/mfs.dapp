import React from 'react'
import { withDirectory } from '../directory'
import Item from './item'

function DirectoryList( props ) {
  const { list } = props.withDirectory

  const itemList = list.map( el => (
    <Item key={ el.name + el.id } it={ el } />
  ) )

  return (
    <main className="column">
      <ul>
        { itemList }
      </ul>
    </main>
  )
}

export default withDirectory( DirectoryList )
