import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withDirectory } from '../../directory'
import DirectoryActionMenu from '../../directoryActionMenu'

import { FiFolder, FiFile, FiMoreVertical } from 'react-icons/fi'
import { FaRegCircle, FaCircle } from 'react-icons/fa'

function Item( props ) {
  const { it } = props
  const { isPick } = props.withDirectory
  const [ showActionMenu, setShowActionMenu ] = useState( false )

  function open() {
    const { type } = it

    if ( type === 1 )
      return openDir()

    openFile()
  }

  function openDir() {
    const { buildPath, cd } = props.withDirectory
    const to = buildPath( it.name )
    cd( to )
  }

  function openFile() {
    const address = 'http://127.0.0.1:8080/ipfs/' + it.id
    window.open( address )
  }

  function toggleMenu() {
    setShowActionMenu( !showActionMenu )
  }

  const icon = isPick ? it.selected ? <FaCircle /> : <FaRegCircle /> : it.type ? <FiFolder /> : <FiFile />

  return (
    <>
      <li>
        <figure onClick={ it.pick }>
          { icon }
        </figure>

        <label onClick={ open }>
          { it.name }
        </label>

        <button onClick={ toggleMenu }>
          <FiMoreVertical />
        </button>
      </li>

      { showActionMenu ? (
        <DirectoryActionMenu
          item={ it }
          closeIt={ toggleMenu }
        />
      ) : null }
    </> 
  )
}

Item.propTypes = {
  it: PropTypes.object.isRequired
}

export default withDirectory( Item )
