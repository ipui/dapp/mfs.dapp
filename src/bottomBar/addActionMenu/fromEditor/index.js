import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withIpfs } from '../../../ipfs'
import RouteTools from '../../../lib/RouteTools'
import Modal from '../../../modal'

import { FiX } from 'react-icons/fi'
import { BsCursorText } from 'react-icons/bs'

class FromIpfs extends Component {
  state = {
    showModal: false,
    model: {
      name: '',
      invalid: true
    },
  }

  static propTypes = {
    closeIt: PropTypes.func.isRequired
  }

  constructor( props ) {
    super( props )
    this.toggleModal = this.toggleModal.bind( this )
    this.updateName = this.updateName.bind( this )
    this.create = this.create.bind( this )
  }

  toggleModal() {
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      showModal: !prevState.showModal
    } ) )
  }

  updateModel( value ) {
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      model: {
        ...prevState.model,
        ...value
      }
    } ) )
  }

  updateName( event ) {
    const { value } = event.target
    const valid = value.length > 0
    this.updateModel( {
      name: value,
      invalid: !valid
    } )
  }

  create() {
    const { model } = this.state
    const { app } = RouteTools.parse().addresses
    const fullpath = RouteTools.concat( RouteTools.join( app, model.name ) )
    RouteTools._go( 'https://editor.octoz.app/' + fullpath )
  }

  render() {
    const {
      model,
      showModal
    } = this.state

    return (
      <>
        <li onClick={ this.toggleModal }>
          <figure>
            <BsCursorText />
          </figure>
          <label>from editor</label>
        </li>

        { showModal ? (
          <Modal closeWith={ this.toggleModal }>
            <header>
              <h1>create file</h1>

              <button onClick={ this.toggleModal }>
                <FiX />
              </button>
            </header>

            <main>
              <div className="row">
                <input
                  type="text"
                  placeholder="name"
                  value={ model.name }
                  onChange={ this.updateName }
                  size="1"
                  autoFocus
                />
              </div>
            </main>

            <footer>
              <button className="full" onClick={ this.create } disabled={ model.invalid }>
                create
              </button>
            </footer>
          </Modal>
        ) : null }
      </>
    )
  }
}

export default withIpfs( FromIpfs )
