import React from 'react';
import PropTypes from 'prop-types';
import './fromDisk.sass';
import { withDirectory } from '../../../directory';

function FromDisk( props ) {
  function buildPath( file ) {
    const { buildPath: bp } = props.withDirectory;
    const { name, webkitRelativePath } = file;

    if ( webkitRelativePath !== '' )
      return bp( webkitRelativePath )

    return bp( name );
  }

  function writeFile( filename, content ) {
    const { api } = props.withDirectory;

    return api.write( filename, new Buffer( content ), {
      create: true,
      parents: true
    } )
  }

  function uploadFile( file ) {
    if ( file instanceof File === false ) throw new Error( 'not a file' );

    return new Promise( ( resolve, reject ) => {
      const reader = new FileReader();

      reader.onload = ( event ) => {
        const content = event.target.result;
        const filename = buildPath( file );
        resolve( writeFile( filename, content ) );
      }

      reader.onerror = reject;

      reader.readAsArrayBuffer( file );
    } );
  }

  function upload( event ) {
    const { files } = event.target;

    if ( files instanceof FileList === false ) throw new Error( 'not a list of files' );

    const filePromises = []
    for ( let i = 0; i < files.length; i++ ) {
      filePromises.push( uploadFile( files[ i ] ) );
    }

    filePromises.reduce( ( chain, current ) => {
      return chain.then( chainResult =>
        current.then( currentResult =>
          [ ...chainResult, currentResult ]
        )
      )
    }, Promise.resolve( [] ) )
    .then( results => {
      props.withDirectory.refresh();
      props.closeIt();
    } )
  }

  const { allowDirectory, children } = props;
  const icon = children.filter( el => el.type === 'figure' ).shift();
  const label = children.filter( el => el.type === 'label' ).shift();

  return (
    <li>
      { icon ? ( icon ) : null }
      <label className="add-from-disk">
        { allowDirectory ? (
          <input
            type="file"
            onChange={ upload }
            mozdirectory="true"
            webkitdirectory="true"
            directory="true"
          />
        ) : (
          <input
            type="file"
            onChange={ upload }
            multiple
          />
        ) }
        { label ? ( label.props.children ) : null }
      </label>
    </li>
  );
}

FromDisk.propTypes = {
  allowDirectory: PropTypes.bool,
  closeIt: PropTypes.func.isRequired,
  children: PropTypes.array
}

export default withDirectory( FromDisk );
