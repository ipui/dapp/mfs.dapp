import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CID from 'cids';
import { withIpfs } from '../../../ipfs';
import { withDirectory } from '../../../directory';
import Modal from '../../../modal';

import { FiX, FiHexagon } from 'react-icons/fi';
import { BiHelpCircle } from 'react-icons/bi';

class FromIpfs extends Component {
  state = {
    showHelp: false,
    showModal: false,
    model: {
      address: '',
      name: '',
      valid: false,
      invalid: true
    },
  }

  static propTypes = {
    closeIt: PropTypes.func.isRequired
  }

  constructor( props ) {
    super( props );
    this.toggleHelp = this.toggleHelp.bind( this );
    this.toggleModal = this.toggleModal.bind( this );
    this.updateAddress = this.updateAddress.bind( this );
    this.add = this.add.bind( this );
  }

  toggleHelp() {
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      showHelp: !prevState.showHelp
    } ) )
  }

  toggleModal() {
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      showModal: !prevState.showModal
    } ) )
  }

  parseAddress( source ) {
    const re = /^(\/ipfs\/)?([a-zA-Z0-9]*)\/?.*/
    const [ input, prefix = '', cid ] = source.match( re ) || []

    if ( !cid ) return

    const postfix = input.slice( ( prefix + cid ).length )

    return {
      input,
      prefix,
      cid,
      postfix
    }
  }

  buildFrom() {
    const { address } = this.state.model;
    const parsed = this.parseAddress( address );

    if ( !parsed.prefix )
      return '/ipfs/' + address;

    return address
  }

  add() {
    const { invalid } = this.state.model;
    if ( invalid ) return;

    const { node: ipfs } = this.props.withIpfs;
    const { api, buildPath, refresh } = this.props.withDirectory;
    const { closeIt } = this.props;

    const { address, name } = this.state.model;

    ipfs
      .object.stat( address, { timeout: '10s' } )
      .then( ( { Hash } ) => {
        const fullpath = buildPath( name || Hash )
        const from = this.buildFrom();
        return api.cp( from, fullpath, { parents: true } )
      } )
      .then( () => {
        refresh();
        closeIt();
      } )
      .catch( console.error )
  }

  updateModel( value ) {
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      model: {
        ...prevState.model,
        ...value
      }
    } ) )
  }

  updateAddress( event ) {
    const { value } = event.target;
    this.updateModel( { address: value } )
    this.toggleValidAddress( this.validateAddress( value ) );
    const filename = this.getFileName( value );
    this.updateName( filename );
  }

  toggleValidAddress( isValid ) {
    this.updateModel( {
      valid: isValid,
      invalid: !isValid
    } );
  }

  cleanAddress( value ) {
    return value.replace( /\/$/, '' );
  }

  getFileName( value ) {
    const parsed = this.parseAddress( value );
    if ( !parsed ) return;

    const index = this.cleanAddress( value ).lastIndexOf( '/' ) + 1
    const name = this.cleanAddress( value.slice( index ) )

    if ( parsed.cid === name )
      return '';

    return name;
  }

  updateName( value ) {
    this.updateModel( { name: value } );
  }

  validateAddress( value ) {
    const parsed = this.parseAddress( value )
    if ( !parsed ) return

    try {
      const cid = new CID( parsed.cid );

      if ( cid.codec === 'dag-cbor' )
        return false;

      return true;
    } catch ( e ) {
      return false;
    }
  }

  render() {
    const {
      model,
      showModal,
      showHelp
    } = this.state;

    return (
      <>
        <li onClick={ this.toggleModal }>
          <figure>
            <FiHexagon />
          </figure>
          <label>from ipfs</label>
        </li>

        { showModal ? (
          <Modal closeWith={ this.toggleModal }>
            <header>
              <h1>from ipfs</h1>

              <button onClick={ this.toggleHelp }>
                <BiHelpCircle />
              </button>

              <button onClick={ this.toggleModal }>
                <FiX />
              </button>
            </header>

            <main>
              { showHelp ? (
                <>
                  <div className="row">
                    <p>
                      A content identifier, or CID, is a label used to point to material in IPFS. It doesn’t indicate where the content is stored, but it forms a kind of address based on the content itself. <a href="http://127.0.0.1:8080/ipns/docs.ipfs.io/guides/concepts/cid">Content Identifiers</a>
                    </p>
                  </div>
                  <div className="row">
                    <p>
                      examples:
                    </p>
                  </div>
                  <div className="row">
                    <pre className="overflow">
                      /ipfs/QmZTR5bcpQD7cFgTorqxZDYaew1Wqgfbd2ud9QqGPAkK2V
                      QmPZ9gcCEpqKTo6aq61g2nXGUhM4iCL3ewB6LDXZCtioEB
                    </pre>
                  </div>
                </>
              ) : null }

              <div className="row">
                <input
                  type="text"
                  placeholder="Qm..."
                  value={ model.address }
                  onChange={ this.updateAddress }
                  size="1"
                  autoFocus
                />
              </div>

              { model.valid ? (
                <div className="row">
                  <input
                    type="text"
                    placeholder="rename"
                    size="1"
                    value={ model.name }
                    onChange={ e => this.updateName( e.target.value ) }
                  />
                </div>
              ) : null }
            </main>

            <footer>
              <button className="full" onClick={ this.add } disabled={ model.invalid }>
                import
              </button>
            </footer>
          </Modal>
        ) : null }
      </>
    );
  }
}

export default withIpfs( withDirectory( FromIpfs ) );
