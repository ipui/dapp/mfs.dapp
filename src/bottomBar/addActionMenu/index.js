import React from 'react'
import Menu from '../../menu'

import {
  FiX,
  FiFile,
  FiFolder
} from 'react-icons/fi'

import { BsCursorText } from 'react-icons/bs'

import FromEditor from './fromEditor'
import FromDisk from './fromDisk'
import FromIpfs from './fromIpfs'

function AddActionMenu( props ) {
  const { closeIt } = props

  return (
    <Menu closeWith={ closeIt }>
      <header>
        <h1>add</h1>
        <button onClick={ closeIt }>
          <FiX />
        </button>
      </header>
      <ul>
        <FromEditor closeIt={ closeIt }>
          <figure>
            <BsCursorText />
          </figure>
          <label>from editor</label>
        </FromEditor>

        <FromDisk closeIt={ closeIt }>
          <figure>
            <FiFile />
          </figure>
          <label>file</label>
        </FromDisk>

        <FromDisk closeIt={ closeIt } allowDirectory={ true }>
          <figure>
            <FiFolder />
          </figure>
          <label>folder</label>
        </FromDisk>

        <FromIpfs closeIt={ closeIt } />

      </ul>
    </Menu>
  )
}

export default AddActionMenu
