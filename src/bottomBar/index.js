import React, { useState } from 'react'
import { withDirectory } from '../directory'
import NewFolder from './newFolder'
import AddActionMenu from './addActionMenu'
import PickActions from './pickActions'
import ClipWindow from './clipWindow'

import {
  FiPlus,
  FiFolderPlus
} from 'react-icons/fi'

function BottomBar( props ) {
  const [ bar, setBar ] = useState( null )
  const { isPick } = props.withDirectory

  return (
    <>
      { isPick ? (
        <PickActions />
      ) : (
        <>
          { bar === 'newFolder' ? (
            <NewFolder closeIt={ () => setBar( null ) } />
          ) : null }

          { bar === 'add' ? (
            <AddActionMenu closeIt={ () => setBar( null ) } />
          ) : null }

          { bar === null ? (
            <footer className="right">
              <button onClick={ () => setBar( 'add' ) }>
                <FiPlus />
              </button>

              <button onClick={ () => setBar( 'newFolder' ) }>
                <FiFolderPlus />
              </button>

              <ClipWindow />
            </footer>
          ) : null }
        </>
      ) }
    </>
  )
}

export default withDirectory( BottomBar )
