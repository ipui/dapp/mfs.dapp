import React, { Component } from 'react'
import { withDirectory } from '../../../directory'

import { FiTrash, FiAlertOctagon } from 'react-icons/fi'

class Remove extends Component {
  state = {
    showConfirm: false
  }

  timeout = undefined

  constructor( props ) {
    super( props )
    this.action = this.action.bind( this )
    this.toggle = this.toggle.bind( this )
    this.waitConfirm = this.waitConfirm.bind( this )
  }

  componentWillUnmount() {
    clearTimeout( this.timeout );
  }

  action() {
    const { list, api, refresh, buildPath, all, closePick } = this.props.withDirectory
    list
      .filter( el => el.selected )
      .map( el => {
        const fullpath = buildPath( el.name )
        return api.rm( fullpath, { recursive: true } )
      } )
      .reduce( ( chain, current ) => {
        return chain.then( chainResult =>
          current.then( currentResult =>
            [ ...chainResult, currentResult ]
          )
        )
      }, Promise.resolve( [] ) )
      .then( results => {
        refresh()
        all( false )
        closePick()
      } )
  }

  waitConfirm() {
    const { delay = 2000 } = this.props
    this.toggle()
    this.timeout = setTimeout( () => { this.toggle() }, delay )
  }

  toggle() {
    this.setState( ( prevState, props ) => ( { showConfirm: !prevState.showConfirm } ) )
  }

  render() {
    const { showConfirm } = this.state

    return (
      <>
        { showConfirm ? (
          <button title="confirm" onClick={ this.action }>
            <FiAlertOctagon />
          </button>
        ) : (
          <button title="remove" onClick={ this.waitConfirm }>
            <FiTrash />
          </button>
        ) }
      </>
    )
  }
}

export default withDirectory( Remove )
