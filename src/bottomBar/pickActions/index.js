import React from 'react'
import { withDirectory } from '../../directory'
import { withClipboard } from '../../clipboard'
import Remove from './remove'

import { FiCopy, FiScissors } from 'react-icons/fi'

function PickActions( props ) {
  function getSelected() {
    const { list } = props.withDirectory
    return list.filter( el => el.selected )
  }

  function addToClip( items, cut = false ) {
    const { buildPath, closePick, all } = props.withDirectory
    const { put } = props.withClipboard
    items
      .map( el => ( {
        name: el.name,
        type: el.type,
        fullpath: buildPath( el.name ),
        cut
      } ) )
      .forEach( el => put( el ) )

    all( false )
    closePick()
  }

  function copy() {
    const toCopy = getSelected()
    addToClip( toCopy )
  }

  function cut() {
    const toCut = getSelected()
    addToClip( toCut, true )
  }

  return (
    <footer>
      <button title="copy" onClick={ copy }>
        <FiCopy />
      </button>

      <button title="cut" onClick={ cut }>
        <FiScissors />
      </button>

      <Remove />
    </footer>
  )
}

export default withDirectory( withClipboard(  PickActions ) )
