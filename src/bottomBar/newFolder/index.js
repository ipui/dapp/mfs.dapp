import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withDirectory } from '../../directory';
import InlineForm, { InlineFormStep } from '../../inlineForm';

import {
  FiCheck,
  FiX
} from 'react-icons/fi';

function NewFolder( props ) {
  const [ name, setName ] = useState( '' );

  function createFolder() {
    const { buildPath, refresh, api } = props.withDirectory;
    const { closeIt } = props;

    const dirName = buildPath( name );

    api
      .mkdir( dirName, { parents: true } )
      .then( () => {
        refresh();
        closeIt();
      } )
      .catch( console.error )
  }

  function reset() {
    const { closeIt } = props;
    closeIt();
  }

  return (
    <>
      <InlineForm onSubmit={ createFolder } onReset={ reset }>
        <InlineFormStep>
          <footer>
            <input
              type="text"
              placeholder="name"
              value={ name }
              onChange={ e => setName( e.target.value ) }
              autoFocus
              required
            />

            <button type="submit">
              <FiCheck />
            </button>

            <button type="reset">
              <FiX />
            </button>
          </footer>
        </InlineFormStep>
      </InlineForm>
    </>
  );
}

NewFolder.propTypes = {
  closeIt: PropTypes.func.isRequired
};

export default withDirectory( NewFolder );
