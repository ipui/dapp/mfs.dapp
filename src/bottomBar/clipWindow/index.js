import React, { useState } from 'react'
import { withDirectory } from '../../directory'
import { withClipboard } from '../../clipboard'
import Item from './item'
import Modal from '../../modal'

import {
  FiClipboard,
  FiMinus,
  FiMaximize2,
  FiMinimize2,
  FiTrash
} from 'react-icons/fi'

import { BiPaste } from 'react-icons/bi'

function ClipWindow( props ) {
  const [ layout, setLayout ] = useState( 'bottom' )
  const [ showClipWindow, setShowClipWindow ] = useState( false )

  function toggleClipWindow() {
    setShowClipWindow( !showClipWindow )
  }

  function toggleLayout() {
    if ( layout === 'bottom' )
      return setLayout( 'fullscreen' )

    setLayout( 'bottom' )
  }

  function pasteAll() {
    const { clip, clean } = props.withClipboard
    const { api, buildPath, refresh } = props.withDirectory

    clip
      .map( el => {
        const { fullpath: from, name } = el
        const to = buildPath( name )
        const fn = el.cut ? api.mv : api.cp
        return fn( from, to, { parents: true } )
      } )
      .reduce( ( chain, current ) => {
        return chain.then( chainResult =>
          current.then( currentResult =>
            [ ...chainResult, currentResult ]
          )
        )
      }, Promise.resolve( [] ) )
      .then( results => {
        refresh()
        clean()
      } )
  }

  const { clip, clean } = props.withClipboard

  const clipItems = clip.map( el => (
    <Item key={ el.name + el.fullpath  } it={ el } />
  ) )

  return (
    <>
      { clip.length > 0 ? (
        <>
          <button onClick={ toggleClipWindow }>
            <FiClipboard />

            <sup>
              { clip.length }
            </sup>
          </button>

          { showClipWindow ? (
            <Modal
              layout={ layout + ' no-padding' }
              closeWith={ toggleClipWindow }
            >
              <header>
                <button onClick={ clean }>
                  <FiTrash />
                </button>

                <button onClick={ pasteAll }>
                  <BiPaste />
                </button>

                <div className="spacer" />

                <button onClick={ toggleClipWindow }>
                  <FiMinus />
                </button>

                <button onClick={ toggleLayout }>
                  { layout === 'bottom' ? (
                    <FiMaximize2 />
                  ) : (
                    <FiMinimize2 />
                  ) }
                </button>
              </header>

              <main>
                <ul>
                  { clipItems }
                </ul>
              </main>
            </Modal>
          ) : null }
        </>
      ) : null }
    </>
  )
}

export default withDirectory( withClipboard( ClipWindow ) )
