import React from 'react'
import { withDirectory } from '../../../directory'
import { withClipboard } from '../../../clipboard'

import { FiFolder, FiFile, FiTrash } from 'react-icons/fi'
import { BiPaste } from 'react-icons/bi'

function Item( props ) {
  const { it } = props

  function getIndex() {
    const { clip } = props.withClipboard
    clip.indexOf( it )
  }

  function remove() {
    const { remove: rm } = props.withClipboard
    const index = getIndex()
    rm( index )
  }

  async function paste() {
    const { api, buildPath, refresh } = props.withDirectory
    const { fullpath: from } = it
    const to = buildPath( it.name )
    const fn = it.cut ? api.mv : api.cp

    fn( from, to, { parents: true } )
      .then( () => {
        remove()
        refresh()
      } )
      .catch( console.warn )
  }

  return (
    <li>
      <figure>
        { it.type ? ( <FiFolder /> ) : ( <FiFile /> ) }
      </figure>

      <label>{ it.name }</label>

      <button title="remove" onClick={ remove }>
        <FiTrash />
      </button>

      <button title="paste" onClick={ paste }>
        <BiPaste />
      </button>
    </li>
  )
}

export default withDirectory( withClipboard( Item ) )
