import React from 'react'
import Menu from '../menu'

import CopyCid from './copyCid'
import OpenOnGateway from './openOnGateway'
import OpenOnEditor from './openOnEditor'
import Rename from './rename'
import Remove from './remove'

import { FiX } from 'react-icons/fi'

function DirectoryActionMenu( props ) {
  const { item, closeIt = () => {} } = props

  return (
    <Menu closeWith={ closeIt }>
      <header>
        <h1>{ item.name }</h1>
        <button onClick={ closeIt }>
          <FiX />
        </button>
      </header>
      <ul>
        <CopyCid item={ item } close={ closeIt } />

        <OpenOnEditor item={ item } close={ closeIt } />

        <OpenOnGateway item={ item } close={ closeIt } />

        <Rename item={ item } close={ closeIt } />

        <Remove item={ item } close={ closeIt } />
      </ul>
    </Menu>
  )
} 

export default DirectoryActionMenu
