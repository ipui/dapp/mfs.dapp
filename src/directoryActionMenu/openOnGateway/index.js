import React from 'react'
import PropTypes from 'prop-types'

import { FiGlobe } from 'react-icons/fi'

function OpenOnGateway( props ) {
  const { item, close } = props

  function share() {
    const win = window.open( 'https://gateway.rabrux.space/ipfs/' + item.id, '_blank' )
    win.focus()
    close()
  }

  if ( item.type === 0 ) return null

  return (
    <li onClick={ share }>
      <figure>
        <FiGlobe />
      </figure>
      <label>open on gateway</label>
    </li>
  )
}

OpenOnGateway.propTypes = {
  item: PropTypes.object.isRequired,
  close: PropTypes.func
}

export default OpenOnGateway
