import React from 'react';
import PropTypes from 'prop-types';
import copyToClipboard from '../../lib/copyToClipboard';

import { FiClipboard } from 'react-icons/fi';

function CopyCid( props ) {
  const { item, close } = props;

  function copy() {
    copyToClipboard( item.id );
    close();
  }

  return (
    <li onClick={ copy }>
      <figure>
        <FiClipboard />
      </figure>
      <label>copy cid</label>
    </li>
  );
}

CopyCid.propTypes = {
  item: PropTypes.object.isRequired,
  close: PropTypes.func
}

export default CopyCid;
