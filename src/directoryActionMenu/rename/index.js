import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withDirectory } from '../../directory';
import Modal from '../../modal';

import { FiCheck, FiX } from 'react-icons/fi';
import { CgRename } from 'react-icons/cg'

function Rename( props ) {
  const [ showForm, setShowForm ] = useState( false );
  const [ newName, setNewName ] = useState( '' );

  const { item, close } = props;

  function rename() {
    const { name } = item;
    const { api, refresh, buildPath } = props.withDirectory;

    const from = buildPath( name );
    const to = buildPath( newName );

    api
      .mv( from, to )
      .then( () => {
        refresh();
        close();
      } )
      .catch( console.error )
  }

  function toggle() {
    setShowForm( !showForm );
  }

  return (
    <>
      <li onClick={ toggle }>
        <figure>
          <CgRename />
        </figure>
        <label>rename</label>
      </li>

      { showForm ? (
        <Modal closeWith={ toggle }>
          <header>
            <h1>{ item.name }</h1>
            <button onClick={ toggle }>
              <FiX />
            </button>
          </header>

          <main>
            <div className="row">
              <input
                type="text"
                placeholder="new name"
                value={ newName }
                onChange={ e => setNewName( e.target.value ) }
                size="1"
                autoFocus
              />
            </div>
          </main>

          <footer>
            <button className="full" onClick={ rename }>
              <FiCheck />
              <label>rename</label>
            </button>
          </footer>
        </Modal>
      ) : null }
    </>
  );
}

Rename.propTypes = {
  item: PropTypes.object.isRequired,
  close: PropTypes.func
}

export default withDirectory( Rename );
