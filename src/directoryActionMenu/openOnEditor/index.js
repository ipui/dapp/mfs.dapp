import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as FileType from 'file-type'
import { withIpfs } from '../../ipfs'
import RouteTools from '../../lib/RouteTools'
import Iterator from '../../lib/Iterator'

import { BsCursorText } from 'react-icons/bs'

class OpenOnEditor extends Component {
  static propTypes = {
    item: PropTypes.object.isRequired,
    close: PropTypes.func.isRequired
  }

  state = {
    show: false,
    path: undefined
  }

  async componentDidMount() {
    if ( this.props.item.type === 1 ) return

    const { files } = this.props.withIpfs.node
    const { item } = this.props
    const { addresses } = RouteTools.parse()

    const path = RouteTools.join( addresses.app, item.name )

    const headerIterator = files.read( path, { offset: 0, length: 32 } )
    const header = ( await Iterator.reduce( headerIterator, val => {
      return val
    } ) ).shift()

    const type = await FileType.fromBuffer( header )

    if ( !type )
      this.setState( { show: true, path } )
  }

  open() {
    const { path } = this.state
    const fullpath = RouteTools.concat( path )
    RouteTools._go( 'https://editor.octoz.app/' + fullpath )
  }

  render() {
    if ( this.props.item.type === 1 ) return null
    const { show } = this.state

    if ( !show ) return null

    return (
      <li onClick={ this.open.bind( this ) }>
        <figure>
          <BsCursorText />
        </figure>
        <label>open on editor</label>
      </li>
    )
  }
}

export default withIpfs( OpenOnEditor )
