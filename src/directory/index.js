import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'
import RouteTools from '../lib/RouteTools.js'
import { withIpfs } from '../ipfs'
import Path from 'path'

const DirectoryContext = createContext()

class Directory extends Component {
  state = {
    current: {
      fullpath: '/',
      dirname: '/',
      name: '/'
    },
    list: [],
    isPick: false
  }

  static propTypes = {
    children: PropTypes.node.isRequired
  }

  constructor( props ) {
    super( props )
    this.refresh = this.refresh.bind( this )
    this.getCurrent = this.getCurrent.bind( this )
    this.fileList = this.fileList.bind( this )
    this.buildPath = this.buildPath.bind( this )
    this.cd = this.cd.bind( this )
    this.all = this.all.bind( this )
    this.closePick = this.closePick.bind( this )
  }

  componentDidMount() {
    const current = this.getCurrent()

    // save current to state
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      current
    } ) )

    this.ls( current.fullpath )
  }

  getCurrent() {
    const { app: fullpath } = RouteTools.parse().addresses

    return {
      fullpath,
      dirname: Path.dirname( fullpath ),
      name: Path.basename( fullpath ) || '/'
    }
  }

  buildPath( ...parts ) {
    const { fullpath } = this.state.current
    return Path.join( fullpath, ...parts )
  }

  pick( item ) {
    const { list } = this.state
    const selectedItem = list.filter( el => el.id === item.id && el.name === item.name ).shift()

    selectedItem.selected = !selectedItem.selected
    const isPick = true

    this.setState( ( prevState, props ) => ( {
      ...prevState,
      list,
      isPick
    } ) )
  }

  all( selected = true ) {
    const list = this.state.list.map( el => ( { ...el, selected } ) )
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      list
    } ) )
  }

  closePick() {
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      isPick: false
    } ) )
  }

  async fileList( iterator ) {
    const item = await iterator.next()
    if ( item.done ) return []

    // cid to id
    item.value.id = item.value.cid.string
    // selected
    item.value.selected = false
    item.value.pick = () => this.pick( item.value )

    return [ item.value, ...( await this.fileList( iterator ) ) ]
  }

  ls( path ) {
    const { node: api } = this.props.withIpfs

    api
      .files.stat( path )
      .then( stats => {
        const { type } = stats

        if ( type !== 'directory' )
          throw new Error( 'this isnt a directory node.' )

        return api.files.ls( path )
      } )
      .then( async ( files ) => {
        const list = await this.fileList( files )
        this.setState( ( prevState, props ) => {
          return { ...prevState, list }
        } )
      } )
      .catch( console.error )
  }

  cd( path ) {
    RouteTools.go( path )
    const current = this.getCurrent()

    // save current to state
    this.setState( ( prevState, props ) => ( {
      ...prevState,
      current
    } ) )

    this.ls( current.fullpath )
  }

  refresh() {
    const { fullpath } = this.state.current
    this.ls( fullpath )
  }

  render() {
    const { current, list, isPick } = this.state
    const { refresh, buildPath, cd, all, closePick } = this

    return (
      <DirectoryContext.Provider value={ {
        current,
        list,
        isPick,
        all,
        closePick,
        refresh,
        buildPath,
        cd,
        api: { ...this.props.withIpfs.node.files }
      } }>
        { this.props.children }
      </DirectoryContext.Provider>
    )
  }

}

function withDirectory( ComponentAlias ) {
  return props => (
    <DirectoryContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withDirectory={ context } />
      } }
    </DirectoryContext.Consumer>
  )
}

export default withIpfs( Directory )

export {
  withDirectory
}
